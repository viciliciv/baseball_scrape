# Baseball #
## Scrape from fangraphs ##

FanGraph Scrape.R - Scrapes Regular split from Fangraph from 1975 - 2015:
* library('RSelenium')
* library('dplyr')
* library('httr')
* library('stringr')
* Requires Mozilla Firefox and setting up a Firefox profile. http://toolsqa.com/selenium-webdriver/custom-firefox-profile/
* Reference: http://www.astrometsmind.com/2016/04/Scrape-Fangraphs-Stats-RStudio.html